#made by Vlad C
#avaliable under GPL v3
import random
import time


pool='1234567890-=`[] \;'',./~!@##$%^&*()_+|}{:"?><>qwertyuiopasdfghjklzxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM'
challenge=[]
line_length=10
game=True
chars_disp=10

try:
    while game:

        go_on=False
        again=1
        user_suc=False
        done=False
        current_char=0
        time_taken=0
        error=0
        almost_done=False

        print ("Welcome to INPUY 0.9")
        print ("Press ctrl+c to close \n","<-- means end of line")
        
        while not go_on: #length of challenge
            length=int(input('enter how many symbols to include: '))
            if not type(length)==int:
                print ('input is not a an integer enter it again')
            else:
                go_on=True
        if length<chars_disp:
            chars_disp=length

        for i in range(0,length): #generates a list of char to be displayed
            r=random.randint(0,len(pool)-1)
            challenge.append(pool[r])
        
        
        time_taken=time.time()

        while not done:

            display=[]
            if almost_done==True:
                done=True
            
            for i in range(current_char,current_char+chars_disp): #display next 10 char from challenge
                        display.append(challenge[i])

            current_char+=chars_disp
            if chars_disp+current_char>=len(challenge): #done only when somewhat out of range
                chars_disp=len(challenge)-chars_disp
                if chars_disp>0:
                    almost_done=True
                    
                else:
                    done=True
            
            correct=False
            display=str("".join(display))

            while not correct:
                print('\n'*100)
                print("{}<--".format(display))
                user=input()

                if user==display:
                    correct=True
                else:
                    error+=1
            correct=False

        time_taken=time.time()-time_taken
        print ('time taken: ',time_taken,' characters per minute: ', length/time_taken*60," %error: ", error/length)
        
        while not user_suc:
            try:
                again=int(input('again(1) or quit(0)?'))
                user_suc=True
            except:
                print("invalid input enter 1 or 0")

        if again==0:
            raise KeyboardInterrupt

except KeyboardInterrupt:
    print('\n Thanks for playing!')
else:
    print('Shenanigans happen!')
    